/* получение нескольких элементов (списка)*/
function get_list_of_elements(element) {
    return document.querySelectorAll(element);
}
/* получение одного элемента*/
function get_element(element) {
    return document.querySelector(element);
}
/* вывод набранных чисел на дисплей */
function display_numbers() {
    if (result_number) {
        carrent_number = this.getAttribute("data-num");
        result_number = "";
    }
    else {
        carrent_number += this.getAttribute("data-num");
    }
    entry_field.innerHTML = carrent_number;
}
/* отчистить дисплей при нажатии на кнопку "C" */
function clear() {
    previous_number = '';
    carrent_number = '';
    entry_field.innerHTML = '0';
    equal.setAttribute("data-result", result_number);
}
/* отчистить дисплей при нажатии на кнопку арифметической операции для ввода следующего числа*/
function clear_for_next_number() {
    previous_number = carrent_number;
    carrent_number = "";
    operator = this.getAttribute("data-ops");
    equal.setAttribute("data-result", "");
}
/* вывод на дисплей результата вычислений */
function set_to_display() {
    const prev_num = parseFloat(previous_number), cur_num = parseFloat(carrent_number);
    let res;
    switch (operator) {
        case "divided":
            res = prev_num / cur_num;
            break;
        case "multiply":
            res = prev_num * cur_num;
            break;
        case "minus":
            res = prev_num - cur_num;
            break;
        case "plus":
            res = prev_num + cur_num;
            break;
        default:
            res = cur_num;
    }
    if (!isFinite(res)) {
        result_number = "Error";
    }
    else {
        result_number = res;
    }
    entry_field.innerHTML = result_number;
    equal.setAttribute("data-result", result_number);
    previous_number = '';
    carrent_number = result_number;
}
let entry_field = get_element("#calc-app__new-calc-value");
let num = get_list_of_elements(".calc-app__button_numbers");
let operation = get_list_of_elements(".calc-app__button_ops");
let equal = get_element("#calc-app__button_equel");
let carrent_number = "";
let previous_number = "";
let operator = "";
let result_number = "";
for (let i = 0; i < num.length; i++) {
    num[i].addEventListener("click", display_numbers);
}
for (let i = 0; i < operation.length; i++) {
    operation[i].addEventListener("click", clear_for_next_number);
}
equal.addEventListener("click", set_to_display);
get_element('#calc-app__button_C').addEventListener("click", clear);
