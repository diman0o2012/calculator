
 /* получение нескольких элементов (списка)*/
 function get_list_of_elements(element: string): NodeListOf<Element> {
     return document.querySelectorAll(element);
 }
 
 /* получение одного элемента*/
 function get_element(element: string): Element {
     return document.querySelector(element);
 }
 
 /* вывод набранных чисел на дисплей */
 function display_numbers(): void {
     if (result_number){
        carrent_number=this.getAttribute("data-num");
        result_number=""
     } 
     else{
        carrent_number+=this.getAttribute("data-num");
     }
     entry_field.innerHTML=carrent_number;
 }
 
/* отчистить дисплей при нажатии на кнопку "C" */
function clear() {
    previous_number='';
     carrent_number='';
     entry_field.innerHTML='0';
     equal.setAttribute("data-result", result_number);
 }

 /* отчистить дисплей при нажатии на кнопку арифметической операции для ввода следующего числа*/
 function clear_for_next_number(): void{
    previous_number=carrent_number;
     carrent_number="";
     operator=this.getAttribute("data-ops");
     equal.setAttribute("data-result", "");
 }
 
 /* вывод на дисплей результата вычислений */
 function set_to_display() {
     const prev_num: number=parseFloat(previous_number),
         cur_num: number=parseFloat(carrent_number);
     let res: number;
     switch (operator) {
        case "divided":
            res=prev_num/cur_num;
            break;

        case "multiply":
             res=prev_num*cur_num;
             break;

         case "minus":
             res=prev_num-cur_num;
             break;
 
         case "plus":
             res=prev_num+cur_num;
             break;

         default:
             res=cur_num;
     }
     if (!isFinite(res)) {
        result_number="Error";
     } 
     else {
        result_number=res as unknown as string;
     }
     entry_field.innerHTML = result_number;
     equal.setAttribute("data-result", result_number);
     previous_number='';
     carrent_number=result_number;
 }

 let entry_field: Element=get_element("#calc-app__new-calc-value");
 let num: NodeListOf<Element>=get_list_of_elements(".calc-app__button_numbers");
 let operation: NodeListOf<Element>=get_list_of_elements(".calc-app__button_ops");
 let equal: Element=get_element("#calc-app__button_equel");
 let carrent_number: string="";
 let previous_number: string="";
 let operator: string="";
 let result_number: string="";

 for (let i : number=0; i < num.length; i++) {
     num[i].addEventListener("click", display_numbers);
 }
 for (let i : number=0; i < operation.length; i++) {
    operation[i].addEventListener("click", clear_for_next_number)
 }
 
 equal.addEventListener("click", set_to_display);

 get_element('#calc-app__button_C').addEventListener("click", clear);